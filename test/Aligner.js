const fs = require('fs');
const path = require('path');
const { expect } = require('chai');
const Aligner = require('../src/Aligner');

const gizaOutputPrefix = 'test';
const testOutputDirName = path.resolve(__dirname, 'output');
const sourceFileName = 'srcTest';
const targetFileName = 'trgTest';
const dictionarySourceFileName = 'dictTest';
const sourceVcbFileName = `${sourceFileName}.vcb`;
const targetVcbFileName = `${targetFileName}.vcb`;
const sntSrcTrgFileName = `${sourceFileName}_${targetFileName}.snt`;
const sntTrgSrcFileName = `${targetFileName}_${sourceFileName}.snt`;
const dictionaryFileName = `${dictionarySourceFileName}.gd`;
let aligner = null;

describe('Aligning', () => {
  before(() => {
    if (!fs.existsSync(testOutputDirName)) {
      fs.mkdirSync(testOutputDirName);
    }
    fs.writeFileSync(path.join(testOutputDirName, sourceFileName), 'I');
    fs.writeFileSync(path.join(testOutputDirName, targetFileName), 'Я');
    fs.writeFileSync(
      path.join(testOutputDirName, dictionarySourceFileName),
      '[{"ru":"Я","en":"I"}]',
    );
  });

  beforeEach(() => {
    aligner = new Aligner({
      source: path.join(testOutputDirName, sourceFileName),
      target: path.join(testOutputDirName, targetFileName),
    });
  });

  afterEach(() => {
    aligner = null;
  });

  after(() => {
    fs.readdir(testOutputDirName, (err, files) => {
      if (!err) {
        files.forEach(fileName => fs.unlinkSync(path.join(testOutputDirName, fileName)));
      }

      fs.rmdirSync(testOutputDirName);
    });
  });

  it('convertCorporas should create 2 .vcb and 2 .snt file and have vcb data', (done) => {
    aligner.convertCorporas()
      .then(() => {
        expect(aligner.sourceVcb).to.be.a('Array').with.lengthOf(1);
        expect(aligner.targetVcb).to.be.a('Array').with.lengthOf(1);
        expect(fs.existsSync(path.join(testOutputDirName, sourceVcbFileName))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, targetVcbFileName))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, sntSrcTrgFileName))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, sntTrgSrcFileName))).to.equal(true);
        done();
      })
      .catch(done);
  });

  it('should parse dictionary file', () => {
    aligner.parseDictionary(path.join(testOutputDirName, dictionarySourceFileName));
    expect(aligner.dictionary).to.be.a('Object').with.property('data');
    expect(aligner.dictionary.data).to.be.a('Array').with.lengthOf(1);
    expect(aligner.dictionary.data[0]).to.have.property('ru');
    expect(aligner.dictionary.data[0]).to.have.property('en');
  });

  it('convertDictionary without initialized dictionary should throw TypeError', () => {
    expect(aligner.convertDictionary.bind(aligner)).to.throw();
  });

  it('convertDictionary should create .gd file', (done) => {
    aligner.convertCorporas()
      .then(() => aligner.parseDictionary(path.join(testOutputDirName, dictionarySourceFileName)))
      .then(() => aligner.convertDictionary('en', 'ru'))
      .then(() => {
        const dictOutput = path.join(testOutputDirName, dictionaryFileName);
        expect(fs.existsSync(dictOutput)).to.equal(true);
        const data = fs.readFileSync(dictOutput, { encoding: 'utf-8' });
        expect(data).to.equal('2 2');
        done();
      })
      .catch(done);
  });

  it('trainClasses should create .vcb.classes and vcb.classes.cats files for source and target', (done) => {
    aligner.trainClasses()
      .then(() => {
        expect(fs.existsSync(path.join(testOutputDirName, `${sourceVcbFileName}.classes`))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, `${sourceVcbFileName}.classes.cats`))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, `${targetVcbFileName}.classes`))).to.equal(true);
        expect(fs.existsSync(path.join(testOutputDirName, `${targetVcbFileName}.classes.cats`))).to.equal(true);
        done();
      })
      .catch(done);
  });

  it('alignCorporas shoult create 17 files', (done) => {
    aligner.convertCorporas()
      .then(() => aligner.parseDictionary(path.join(testOutputDirName, dictionarySourceFileName)))
      .then(() => aligner.convertDictionary('en', 'ru'))
      .then(() => aligner.trainClasses())
      .then(() => aligner.alignCorporas(gizaOutputPrefix, testOutputDirName))
      .then(() => {
        const files = fs.readdirSync(testOutputDirName);
        let counter = 0;
        files.forEach((fileName) => {
          if ((new RegExp(`^${gizaOutputPrefix}`).test(fileName))) {
            counter += 1;
          }
        });
        expect(counter).to.equal(17);
        done();
      })
      .catch(done);
  });
});
