#!/usr/bin/env node

const program = require('commander');
const path = require('path');
const Aligner = require('./src/Aligner');

program
  .version('1.0.0', '-v --version')
  .option('-s --source [fileName]', 'source corpora')
  .option('-ls --language-source [lang]', 'source language')
  .option('-t --target [fileName]', 'target corpora')
  .option('-lt --language-target [lang]', 'target language')
  .option('-d --dictionary [fileName]', 'dictionary in format [{ [source_lang]: word, [target_lang]: translated_word }]', null)
  .option('-c --classes [number]', 'number of classes for training classification model', 100)
  .option('-i --iterations [number]', 'number of iterations for training classification model', 1)
  .option('-o --output [directory]', 'output directory')
  .parse(process.argv);

function run(params) {
  const source = path.resolve(params.source);
  const target = path.resolve(params.target);
  const dictionary = params.dictionary ? path.resolve(params.dictionary) : null;
  const aligner = new Aligner({ source, target });
  aligner.convertCorporas()
    .then(() => {
      if (dictionary) {
        return aligner.parseDictionary(dictionary)
          .then(() => aligner.converDictionary(params.languageSource, params.languageTarget));
      }
      return null;
    })
    .then(() => aligner.trainClasses(params.classes, params.iterations))
    .then(() => aligner.alignCorporas(Date.now(), params.output));
}

if (require.main === module) {
  run(program);
}

module.exports = run;
