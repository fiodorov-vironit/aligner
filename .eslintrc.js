module.exports = {
  "env": {
    "mocha": true
  },
  "plugins": [
    "mocha"
  ],

  "extends": "airbnb-base",
  "rules": {
    "mocha/no-exclusive-tests": "error",
    "no-unused-vars": [
      "error",
      {
        "varsIgnorePattern": ""
      }
    ]
  }
};
