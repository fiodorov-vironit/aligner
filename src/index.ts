import bodyParser from "body-parser";
import express, { Request, Response } from "express";
import mainRouter from "./routes/mainRouter";
import logger from "./utils/logger";

const app: express.Application = express();
const PORT = process.env.PORT || 3000;

app.set("view engine", "pug");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("*", (req: Request, res: Response, next) => {
    logger.info(`Request: ${req.protocol.toUpperCase()} ${req.method} ` +
        `${res.statusCode} ${req.originalUrl}`);
    next();
});

app.use("/", mainRouter);

const server = app.listen(PORT);

server.on("connect", () => {
    logger.info("New client connected");
});

server.on("close", () => {
    logger.info("Server stopped");
});
