import { Request, Response, Router } from "express";
import processes from "../../models/ProcessList";

const processApiRouter = Router();

processApiRouter.delete("/", (req: Request, res: Response) => {
    processes.delete(req.body.id);
    res.sendStatus(200);
});

export default processApiRouter;
