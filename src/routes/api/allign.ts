import { Request, Response, Router } from "express";
import { AllignProcess } from "../../models/AllignProcess";
import processes from "../../models/ProcessList";
import { allignCorporas } from "../../services/allignCorporas";
import logger from "../../utils/logger";

const dictionaryRouter = Router();

dictionaryRouter.post("/", (req: Request, res: Response) => {
    if (!req.body.id) {
        const errorMsg = "Missing required parameter 'id'";
        res.status(422);
        res.send({
            error: {
                code: "ERR_MISSING_PARAMETER",
                message: errorMsg,
            },
        });
        logger.error(errorMsg);
        return;
    }
    logger.info("fetching data...");
    try {
        const allignProcess = new AllignProcess(
            JSON.stringify(req.body),
            allignCorporas,
            {
                id: [req.body.id],
            });
        res.send({
            data: {
                processId: allignProcess.id,
                status: allignProcess.status,
            },
        });
        processes.push(allignProcess);
        allignProcess.start()
            .catch((error) => logger.error({ error }));
    } catch (error) {
        logger.error({ error });
        res.status(422);
        res.send({
            error: {
                code: "ERR_PROCESS_NOT_CREATED",
                message: error.message,
            },
        });
    }
});

dictionaryRouter.get("/status", (req: Request, res: Response) => {
    const processId = req.query.processId;
    if (!processId) {
        const errorMsg = "Missing required parameter 'processId'";
        res.status(422);
        res.send({
            error: {
                code: "ERR_MISSING_PARAMETER",
                message: errorMsg,
            },
        });
        logger.error(errorMsg);
        return;
    }
    const process = processes.get(processId);
    if (!process) {
        const errorMsg = "No process with this id found";
        res.status(422);
        res.send({
            error: {
                code: "ERR_PROCESS_NOT_FOUND",
                message: errorMsg,
            },
        });
        logger.error(errorMsg);
        return;
    }
    res.send({
        data: {
            elapsedTime: process.elapsedTime,
            result: process.result,
            status: process.status,
        },
    });
});

export default dictionaryRouter;
