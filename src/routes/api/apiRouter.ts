import { Router } from "express";
import allignRouter from "./allign";
import processApiRouter from "./processApi";

const apiRouter = Router();
const routesInfo = [
    { path: "/allign", router: allignRouter },
    { path: "/process", router: processApiRouter },
];

routesInfo.forEach(({ path, router }) => {
    apiRouter.use(path, router);
});

export default apiRouter;
