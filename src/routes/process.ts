import { Request, Response, Router } from "express";
import processes from "../models/ProcessList";

const processRouter = Router();

processRouter.get("/:processId", (req: Request, res: Response) => {
    const proc = processes.get(req.params.processId);
    const result: string[][] = [];
    const lines = proc.result.split("\n")
        .filter((line: string, index: number) => index % 3 !== 0)
        .forEach((line: string, index: number) => {
            if (index % 2 === 0) {
                result.push([ line ]);
            } else {
                result[(index - 1) / 2].push(line);
            }
        });
    res.render("process", { result });
});

export default processRouter;
