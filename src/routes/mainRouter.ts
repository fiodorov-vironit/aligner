import { Router } from "express";
import apiRouter from "./api/apiRouter";
import dashboardRouter from "./dashboard";
import processRouter from "./process";

const mainRouter = Router();
const routesInfo = [
    { path: "/gizaApp", router: dashboardRouter },
    { path: "/gizaApp/api", router: apiRouter },
    { path: "/gizaApp/process", router: processRouter },
];

routesInfo.forEach(({ path, router }) => {
    mainRouter.use(path, router);
});

export default mainRouter;
