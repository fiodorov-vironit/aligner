import {Request, Response, Router} from "express";
import processes from "../models/ProcessList";

const dashboardRouter = Router();

dashboardRouter.get("/", (req: Request, res: Response) => {
    const processList = processes.getAll();
    res.render("dashboard", {
        deleteProcess: (id) => processes.delete(id),
        processList,
        title: "Alligner",
    });
});

export default dashboardRouter;
