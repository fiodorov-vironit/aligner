import {spawn} from "child_process";
import fs from "fs";
import path from "path";
import util from "util";
import logger from "../utils/logger";
import {serialize} from "../utils/serializer";
import {fetchCorpora} from "./fetchCorpora";
import {fetchDictionary} from "./fetchDictionary";

const readDir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);

interface IAlignParams {
    id: number[];

    [key: string]: any;
}

export function allignCorporas(packId: string, params: IAlignParams) {
    const sourcePath = `./alligner-sources/${packId}.source`;
    const targetPath = `./alligner-sources/${packId}.target`;
    const dictionaryPath = `./alligner-sources/${packId}.dictionary`;
    const outputPath = "./alligner-sources";
    return fetchCorpora(params.id, {logger})
        .then((corpora) => {
            if (corpora.isEmpty()) {
                throw new Error("corpora is empty");
            }
            serialize(
                sourcePath,
                corpora.data
                    .map(({source}) => source)
                    .join("\n"),
            );
            serialize(
                targetPath,
                corpora.data
                    .map(({target}) => target)
                    .join("\n"),
            );
            return fetchDictionary(corpora.from, corpora.to, {logger})
                .then((dictionary) => {
                    serialize(dictionaryPath, JSON.stringify(dictionary.data));
                    logger.info("Start aligner");
                    return new Promise((resolve, reject) => {
                        const task = spawn("./node_modules/.bin/alligner", [
                            "--alias", packId,
                            "--dictionary", dictionaryPath,
                            "--language-source", corpora.from,
                            "--language-target", corpora.to,
                            "--output", outputPath,
                            "--source", sourcePath,
                            "--target", targetPath,
                        ]);
                        task.stdout.on("data", (data) => {
                            logger.info(`stdout: ${data}`);
                        });

                        task.stderr.on("data", (data) => {
                            logger.info(`stderr: ${data}`);
                        });

                        task.on("close", (code) => {
                            logger.info(`child process exited with code ${code}`);
                        });
                        task.on("exit", (code) => {
                            if (code !== 0) {
                                reject(code);
                            }
                            resolve(code);
                        });
                    }).then(() => readDir(outputPath)).then((files) => {
                        logger.info("Readed files");
                        let data: Promise<any> | null = null;
                        files.forEach((fileName) => {
                            if (fileName === `${packId}.A3.final`) {
                                data = readFile(path.join(outputPath, fileName), {encoding: "utf-8"});
                            } else if (new RegExp(`${packId}`).test(fileName)) {
                                fs.unlinkSync(path.join(outputPath, fileName));
                            }
                        });
                        return data;
                    });
                });
        });
}
