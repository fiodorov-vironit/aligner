import request from "request";
import {Corpora} from "../models/Corpora";

interface IFetchOptions {
    logger: any;
}

export function fetchCorpora(ids: number[], options: IFetchOptions): Promise<Corpora> {
    return new Promise((resolve, reject) => {
        const {logger} = options;
        const corpora: Corpora = new Corpora();
        const partOffset = 20;
        logger.info("Corpora query start");
        let blocksIndex = 0;
        processBlock(0);

        function processBlock(partId) {
            logger.info("Block idx " + ids[blocksIndex].toString());
            logger.info("Process blocks from " + partId + " to " + (partId + partOffset));
            request.get({
                headers: {
                    Authorization: "EM7kgCp4DduvFfhf3D4ZqWqZUSmdx9RLKDrJXvHEvHTde3pUTyCn" +
                    "6cmwGH7MDU2yEvEJwJ4ezc4JeTL7FQhMsUg6CaWEKLYS73LbA4Qdk2px5TQ63kmPFJHMxr6dkB4F",
                },
                json: true,
                url: "https://backenster.com/v3/api/sources/" + ids[blocksIndex] +
                "?from=" + (partId * 5000 + 1) + "&to=" + ((partId + partOffset) * 5000 + 1),
            }, (err, res, body) => {
                if (err) {
                    reject(new Error("Error request " + err.message));
                } else if (body.err) {
                    if (body.err === "Bad offsets, no data for this offsets.") {
                        blocksIndex++;
                        if (ids.length <= blocksIndex) {
                            logger.info("Corpora query end");
                            resolve(corpora);
                        } else {
                            processBlock(partId + partOffset);
                        }
                    } else {
                        reject(new Error("Error in response " + body.err));
                    }
                } else if (!body.result) {
                    reject(new Error("Bad blocks result"));
                } else {
                    const partsItem = body.result;
                    const sourceData = partsItem.source_data.split("\n");
                    const targetData = partsItem.target_data.split("\n");
                    sourceData.forEach((row, index) => {
                        corpora.push({source: row, target: targetData[index]}, partsItem.from_lang, partsItem.to_lang);
                    });
                    processBlock(partId + partOffset);
                }
            });
        }
    });
}
