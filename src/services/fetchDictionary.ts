import {Pool} from "pg";
import {Dictionary} from "../models/Dictionary";

const languageMap = {
    en: "en_GB",
};

interface IFetchOptions {
    logger: any;
}

export function fetchDictionary(from: string, to: string, options: IFetchOptions): Promise<Dictionary> {
    return new Promise((resolve, reject) => {
        const { logger } = options;
        const PGSQL_POOL_SINGLEWORDS = new Pool({
            database: "singlewords",
            host: "136.243.110.101",
            password: "tzXcZ3RSETwS7jyr",
            port: 5432,
            user: "corporau",
        });
        const dictionary: Dictionary = new Dictionary();
        logger.info("Dictionary query start");
        PGSQL_POOL_SINGLEWORDS.query("SELECT source_data, target_data FROM singlewords.public.single_words " +
                "WHERE from_lang = $1 AND to_lang = $2 ORDER BY source_data ASC", [from, to], (err, result) => {
            if (err) {
                reject(new Error("PgSQG error " + err.message));
            }
            result.rows.forEach((row) => {
                dictionary.push({
                    [from]: row.source_data,
                    [to]: row.target_data,
                });
            });
            logger.info("Dictionary query end");
            resolve(dictionary);
        });
    });
}
