const fs = require('fs');
const { spawn } = require('child_process');

exports.execWithPromise = options => new Promise((resolve, reject) => {
  const { command, args } = options;
  const exec = spawn(command, args);

  exec.stdout.on('data', (data) => {
    console.log(`${command} stdout: ${data}`);
  });

  exec.stderr.on('data', (data) => {
    console.log(`${command} stdout: ${data}`);
  });

  exec.on('close', (code) => {
    if (code !== 0) {
      reject(code);
    }
    resolve(code);
  });
});

exports.readObjectFromFile = fileName => JSON.parse(fs.readFileSync(fileName));

exports.readDataFile = fs.readFileSync;
exports.writeDataFile = fs.writeFileSync;
