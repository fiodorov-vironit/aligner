import fs, { WriteFileOptions } from "fs";
import path from "path";

export function serialize(filePath: string, data: any, options?: WriteFileOptions): void {
    if (!fs.existsSync(path.dirname(filePath))) {
        fs.mkdirSync(path.dirname(filePath));
    }
    fs.writeFileSync(path.resolve(filePath), data, options);
}
