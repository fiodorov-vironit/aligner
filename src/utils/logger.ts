import { createLogger, format, LoggerOptions, transports } from "winston";

const errorReplacer = (value: string | { error: Error }) => {
    if (typeof(value) === "string") {
        return value;
    }
    return value.error.stack;
};
const customFormat = format.printf((info) => {
    return `${info.timestamp} ${info.level}: ${errorReplacer(info.message)}`;
});
const loggerOptions: LoggerOptions = {
    exitOnError: false,
    transports: [
        new transports.Console({
            format: format.combine(
                format.timestamp({ format: "HH:mm:ss"}),
                format.colorize(),
                customFormat,
            ),
            level: "debug",
        }),
        new transports.File({
            filename: "logs/error.log",
            format: format.combine(
                format.timestamp(),
                customFormat,
            ),
            handleExceptions: true,
            level: "error",
        }),
    ],
};
const logger = createLogger(loggerOptions);

export default logger;
