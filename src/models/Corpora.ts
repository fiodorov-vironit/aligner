interface ICorporaRec {
    source: string;
    target: string;
}

export class Corpora {
    private mData: ICorporaRec[];
    private mFrom: string;
    private mTo: string;

    constructor() {
        this.mData = [];
        this.mFrom = "";
        this.mTo = "";
    }

    public push(rec: ICorporaRec, sourceLang: string, targetLang: string): void {
        this.mData.push(rec);
        if (this.from !== "" && this.from !== sourceLang) {
            throw new Error("Different source languages in corpora. Already set " +
                `${this.from} but try to set ${sourceLang}`);
        }
        if (this.to !== "" && this.to !== targetLang) {
            throw new Error("Different target languages in corpora. Already set " +
                `${this.to} but try to set ${targetLang}`);
        }
        this.mFrom = sourceLang;
        this.mTo = targetLang;
    }

    public isEmpty(): boolean {
        return this.mData.length === 0;
    }

    get data(): ICorporaRec[] {
        return this.mData;
    }

    get from(): string {
        return this.mFrom;
    }

    get to(): string {
        return this.mTo;
    }
}
