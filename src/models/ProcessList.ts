import {AllignProcess} from "./AllignProcess";

interface IProcessList {
    [id: string]: AllignProcess;
}

class ProcessList {
    private mData: IProcessList;

    constructor() {
        this.mData = {};
    }

    public push(allignProcess: AllignProcess): void {
        this.mData[allignProcess.id] = allignProcess;
    }

    public get(id: string): AllignProcess {
        return this.mData[id];
    }

    public getAll(): AllignProcess[] {
        return Object.keys(this.mData).map((id: string) => this.mData[id]);
    }

    public delete(id: string): void {
        delete this.mData[id];
    }
}

const processes = new ProcessList();
export default processes;
