interface IWordRec {
    [ lang: string ]: string;
}

export class Dictionary {
    private mData: IWordRec[];

    constructor() {
        this.mData = [];
    }

    public push(wordRec: IWordRec): void {
        this.mData.push(wordRec);
    }

    get data(): IWordRec[] {
        return this.mData;
    }
}
