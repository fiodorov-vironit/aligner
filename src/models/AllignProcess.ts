import crypto from "crypto";

const hash = () => crypto.createHash("sha256");
export enum AlignProcessStatus {
    CREATED = "created",
    FULLFILLED = "fullfilled",
    PENDING = "pending",
    REJECTED = "rejected",
}

type TProcessFunction  = (...args: any[]) => Promise<any>;

interface IProcess {
    fn: TProcessFunction;
    args: any[];
}

export class AllignProcess {
    private mProcessId: string;
    private mStatus: AlignProcessStatus;
    private mProcessFn: IProcess;
    private mResult: any;
    private mStart: number;
    private mEnd: number;

    constructor(info: string, fn: TProcessFunction, ...args: any[]) {
        this.mProcessId = hash().update(info + Date.now()).digest("hex");
        this.mStatus = AlignProcessStatus.CREATED;
        this.mProcessFn = { fn, args };
        this.mResult = null;
        this.mStart = 0;
        this.mEnd = 0;
    }

    public start(): Promise<any> {
        const { fn, args } = this.mProcessFn;
        this.mStatus = AlignProcessStatus.PENDING;
        this.mStart = Date.now();
        return Promise.resolve(fn.apply(this, [this.mProcessId, ...args]))
            .then((result: any) => {
                this.mStatus = AlignProcessStatus.FULLFILLED;
                this.mResult = result;
                this.mEnd = Date.now();
            })
            .catch((error) => {
                this.mStatus = AlignProcessStatus.REJECTED;
                this.mResult = error.message;
                throw error;
            });
    }

    get id(): string {
        return this.mProcessId;
    }

    get status(): AlignProcessStatus {
        return this.mStatus;
    }

    get result(): any {
        return this.mResult;
    }

    get elapsedTime(): number {
        return this.mEnd ? this.mEnd - this.mStart : Date.now() - this.mStart;
    }
}
