const path = require('path');

process.env.NODE_CONFIG_DIR = process.env.NODE_CONFIG_DIR || path.join(__dirname, '../config');
const config = require('config');
const {
  execWithPromise,
  readObjectFromFile,
  readDataFile,
  writeDataFile,
} = require('./utils');

const { plain2snt, mkcls, gizapp } = config.get('GIZA');

function Aligner({ source, target }) {
  this.source = source;
  this.target = target;
}

Aligner.prototype.convertCorporas = function convertCorporas() {
  const parse = (buffer => buffer
    .trim()
    .split('\n')
    .map((line) => {
      const data = line.split(' ');
      return {
        id: data[0],
        word: data[1],
      };
    }));

  return execWithPromise({
    command: path.join(__dirname, '..', plain2snt),
    args: [this.source, this.target],
  })
    .then((result) => {
      this.sourceVcb = parse(readDataFile(`${this.source}.vcb`, { encoding: 'utf-8' }));
      this.targetVcb = parse(readDataFile(`${this.target}.vcb`, { encoding: 'utf-8' }));

      return result;
    });
};

Aligner.prototype.parseDictionary = function parseDictionary(fileName) {
  this.dictionary = {
    file: fileName,
    data: readObjectFromFile(fileName),
  };
};

Aligner.prototype.convertDictionary = function convertDictionary(sourceLang, targetLang) {
  if (!this.dictionary) {
    throw new TypeError('dictionary is not initialized');
  }
  const dictData = {};
  this.targetVcb.forEach(({ id: targetId, word: targetWord }) => {
    const dictPair = this.dictionary.data.find(({ [targetLang]: word }) => word === targetWord);
    if (dictPair) {
      const { id: sourceId = null } = this.sourceVcb.find(
        ({ word }) => dictPair[sourceLang] === word,
      );
      if (sourceId) {
        dictData[targetId] = sourceId;
      }
    }
  });
  let dictDataString = '';
  Object.keys(dictData).forEach((targetId) => {
    dictDataString += `${targetId} ${dictData[targetId]}\n`;
  });
  writeDataFile(`${this.dictionary.file}.gd`, dictDataString.trim());
};

Aligner.prototype.trainClasses = function trainClasses(classes = 100, iterations = 1) {
  const buildArguments = corpora => ([
    `-c${classes}`,
    `-n${iterations}`,
    `-p${corpora}`,
    `-V${corpora}.vcb.classes`,
  ]);

  const command = path.join(__dirname, '..', mkcls);

  return Promise.all([
    execWithPromise({
      command,
      args: buildArguments(this.source),
    }),
    execWithPromise({
      command,
      args: buildArguments(this.target),
    }),
  ]);
};

Aligner.prototype.alignCorporas = function alignCorporas(outputPrefix, outputDirPath = './') {
  const gizaArguments = [
    '-s', `${this.source}.vcb`,
    '-t', `${this.target}.vcb`,
    '-c', path.join(path.dirname(this.source), `${path.basename(this.source)}_${path.basename(this.target)}.snt`),
    '-o', outputPrefix,
    '-outputpath', outputDirPath,
  ];

  if (this.dictionary) {
    gizaArguments.push('-d');
    gizaArguments.push(`${this.dictionary.file}.gd`);
  }

  return execWithPromise({
    command: path.join(__dirname, '..', gizapp),
    args: gizaArguments,
  });
};

module.exports = Aligner;
